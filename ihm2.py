global BOOL_LATEX
BOOL_LATEX = True


from sys import exit
import csv                                          # Import csv
from matplotlib import rc                           # For TeX
import matplotlib.pyplot as plt                     # Plot
import tkinter as tk
from tkinter.filedialog import askopenfilename      # Find file


# IMPORTANT :
# Install python3
# Install Mathplotlib

#######################################################################
class Text4Figure(tk.Frame):
    # Cette classe est l'etape 3 :
    # * Titre
    # * Legende
    # * Choix du rang de donnees a utiliser

    # variables of the object
    Zetitle   = ''
    Zelegend  = ''
    ZeXaxis   = ''
    ZeYaxis   = ''
    ZeRankMin = 0
    ZeRankMax = 0
    sizeX     = 0
    sizeY     = 0
    ppp       = 0
    izok = False
    
    # Constructor
    def __init__(self,master,Xtuple,Ytuple):
        # Window initialisation
        tk.LabelFrame.__init__(self, master, height=400, width=470, \
                               text='Step 3/3'                      \
                               +' :    '                            \
                               +'Additional informations'           \
        )

        # Initialisation of ZerankMax
        global keepRankMax 
        keepRankMax = Xtuple[3]-1
        self.ZeRankMax = Xtuple[3]-1
        

        # # # # # # # GUI # # # # # #
        #---Fields---
        # Input the title..............................................
        self.lbl_ttle=tk.Label(self,                                  \
                               text='Enter a title (*)',              \
                               width=17,                              \
                               height=2                               \
        )
        self.lbl_ttle.grid(row=1,column=1)
        self.entr_ttle=tk.Entry(self,width=35)
        self.entr_ttle.grid(row=1,column=2,columnspan=3)

        # Input the legend............................................
        self.lbl_legend=tk.Label(self,                                \
                                 text='Enter a legend',               \
                                 width=17,                            \
                                 height=2                             \
        )
        self.lbl_legend.grid(row=2,column=1)
        self.entr_legend=tk.Entry(self,width=35)
        self.entr_legend.grid(row=2,column=2,columnspan=3)

        # Input the rank min...........................................
        self.lbl_rkmin=tk.Label(self,                                 \
                                text='Range min\n(default = 1)',       \
                                width=12,                             \
                                height=3                              \
        )
        self.lbl_rkmin.grid(row=3,column=1,rowspan=2)
        self.entr_rkmin=tk.Entry(self,width=5)
        self.entr_rkmin.grid(row=3,column=2)

        # Input the rank max ..........................................
        self.lbl_rkmax=tk.Label(self,                                 \
                                text='Range max\n(default = '          \
                                +str(Xtuple[3])                       \
                                +')',                                 \
                                width=12,                              \
                                height=3                              \
        )
        self.lbl_rkmax.grid(row=3,column=3,rowspan=2)
        self.entr_rkmax=tk.Entry(self,width=5)
        self.entr_rkmax.grid(row=3,column=4)

        
        # Input the name of the X-axis.................................
        self.lbl_Xaxname=tk.Label(self,                               \
                                  text='Name of the X axis (*) :',    \
                                  height=2                            \
        )
        self.lbl_Xaxname.grid(row=7,column=1)
        self.entr_Xaxname=tk.Entry(self,                              \
                                   width=10                           \
        )
        self.entr_Xaxname.grid(row=7,column=2)

        # Input the name of the Y-axis.................................
        self.lbl_Yaxname=tk.Label(self,                                \
                                  text='Name of the Y axis (*) :',    \
                                  height=2                            \
        )
        self.lbl_Yaxname.grid(row=7,column=3)
        self.entr_Yaxname=tk.Entry(self,                              \
                                   width=10                           \
        )
        self.entr_Yaxname.grid(row=7,column=4)

        # Resolution (Width)............................................
        self.lbl_lgdWidth=tk.Label(self,                             \
                                   text='Width of the figure(*) (cm) (def=8):',\
                                   justify='left'                    \
        )
        self.lbl_lgdWidth.grid(row=15,column=1)
        self.entr_Width=tk.Entry(self,width=8)
        self.entr_Width.grid(row=15,column=2)


        # Resolution (Height)............................................
        self.lbl_lgdHeight=tk.Label(self,                             \
                                    text='Height of the figure(*) (cm) (def=5) :',\
                                   justify='left'                    \
        )
        self.lbl_lgdHeight.grid(row=15,column=3)
        self.entr_Height=tk.Entry(self,width=8)
        self.entr_Height.grid(row=15,column=4)

        # Resolution (ppp)............................................
        self.lbl_lgdppp=tk.Label(self,                             \
                                   text='Resolution dpi(*) (def = 260) :',\
                                   justify='left'                    \
        )
        self.lbl_lgdppp.grid(row=16,column=2)
        self.entr_ppp=tk.Entry(self,width=10)
        self.entr_ppp.grid(row=16,column=3)
        
        #--- Buttons---------------------------------------------------
        # Apply........................................................
        self.button_apply=tk.Button(                                  \
                                    self,                             \
                                    text='Apply',                     \
                                    width=10                          \
        )
        self.button_apply.bind('<Button-1>',self.IHM_apply) # Launch function on event
        self.button_apply.grid(row=9,column=3)

        # Display curves...............................................
        self.button_display=tk.Button(self,                           \
                                      text='Plot',                    \
                                      width=13                        \
        )
        self.button_display.bind('<Button-1>',self.IHM_displayCurve)
        self.button_display.grid(row=9,column=5)

        # New curve....................................................
        self.button_newcurve=tk.Button(self,                          \
                                       text='Add curve',              \
                                       width=15                       \
        )
        self.button_newcurve.bind('<Button-1>',self.IHM_newCurve)
        self.button_newcurve.grid(row=9,column=2)
        
        # Exit.........................................................
        self.button_exit=tk.Button(self,                              \
                                   text='Exit',                       \
                                   width=15,                          \
                                   command=exit                       \
        ) #self.master.destroy   
        self.button_exit.grid(row=9,column=1)

        #---Display message---
        # Value of rank min for X:
        self.lbl_lgdXvalueRkMin=tk.Label(self,                        \
                                         text='Value rank min (X) :', \
                                         justify='left'               \
        )
        self.lbl_lgdXvalueRkMin.grid(row=5,column=1)
        self.lbl_XvalueRkMin=tk.Label(self,                           \
                                text=str(Xtuple[4][self.ZeRankMin]),  \
                                justify='left'                        \
        )
        self.lbl_XvalueRkMin.grid(row=5,column=2)

        # Value of rank max for X:
        self.lbl_lgdXvalueRkMax=tk.Label(self,                         \
                                         text='Value rank max (X) :',  \
                                         justify='left'                \
        )
        self.lbl_lgdXvalueRkMax.grid(row=6,column=1)
        self.lbl_XvalueRkMax=tk.Label(self,                           \
                                text=str(Xtuple[4][self.ZeRankMax]),  \
                                justify='left'                        \
        )
        self.lbl_XvalueRkMax.grid(row=6,column=2)
        

        # Value of rank min for Y:
        self.lbl_lgdYvalueRkMin=tk.Label(self,                        \
                                         text='Value rank min (Y) :', \
                                         justify='left'               \
        )
        self.lbl_lgdYvalueRkMin.grid(row=5,column=3)
        self.lbl_YvalueRkMin=tk.Label(self,                            \
                                     text=str(Ytuple[4][self.ZeRankMin]), \
                                      justify='left'                   \
        )
        self.lbl_YvalueRkMin.grid(row=5,column=4)

        # Value of rank max for Y:
        self.lbl_lgdYvalueRkMax=tk.Label(self,                        \
                                         text='Value rank max (Y) :',  \
                                         justify='left'               \
        )
        self.lbl_lgdYvalueRkMax.grid(row=6,column=3)
        self.lbl_YvalueRkMax=tk.Label(self,                            \
                                      text=str(Ytuple[4][self.ZeRankMax]),\
                                      justify='left'                   \
        )
        self.lbl_YvalueRkMax.grid(row=6,column=4)

        
        # Infos........................................................
        self.lbl_info=tk.Label(self,                                  \
                               text='All boxes are optionnal\n'       \
                                    +'Please \'Apply\' before doing other'\
                                    +',\neven if some (or all) boxes are empty',\
                               bg='black',fg='red',                   \
                               width=50,                              \
                               justify='center'                       \
        )
        self.lbl_info.grid(row=10,column=1,columnspan=5,rowspan=3)

        self.lbl_infostar=tk.Label(self,                              \
                               text='(*) : Fill only for the first curve\n'\
                                    +'The other options are for each '\
                                    +'curve',                         \
                               bg='black',fg='pink',                  \
                               width=50,                              \
                               justify='center'                       \
        )
        self.lbl_infostar.grid(row=14,column=1,columnspan=5,rowspan=1)
        
        # Number of values in the data file............................
        self.lbl_nbvalues=tk.Label(self,                              \
                                   text='Nb of points :\n'            \
                                   +str(Xtuple[3])                    \
                                   +'\n(max = '+str(Xtuple[3])+')',     \
                                   justify='center'\
        )
        self.lbl_nbvalues.grid(row=4,column=5,rowspan=3)

        # # # # # End GUI # # # # #
    # End constructor
    
    ##########################   Functions    #########################
    def IHM_apply(self,event): # This function is on the event
        
        # Out : tupleT4F=['title','legend', 'Xaxisname','Yaxisname',rkMin,rkMax,sizeX,sizeY]
        # Variables for the return :
        global tupleT4F
        tupleT4F = []
        
        self.izok = True # We can plot or add another curve
        
        # Initialization of variables
        self.Zetitle   = self.entr_ttle.get()
        self.Zelegend  = self.entr_legend.get()


        # Change the info box
        self.lbl_info['text']='\nChangements are saved\n'
        self.lbl_info['fg']  = 'green'

        # Have we a number ? 
        if len(str(self.entr_rkmin.get()))<1:
            self.ZeRankMin = 0
        else:
            self.ZeRankMin = int(self.entr_rkmin.get())-1
        #EndIF

        if len(str(self.entr_rkmax.get()))<1:
            self.ZeRankMax = keepRankMax
        else:
            self.ZeRankMax = int(self.entr_rkmax.get())-1
        #EndIF

        if self.ZeRankMax < self.ZeRankMin:
            self.lbl_info['text']='\nRange Min > Range Max\n!!!'
            self.lbl_info['fg']  = 'red'
            self.entr_rkmin['color']='red'
            self.entr_rkmax['color']='red'
            
        # Update the informations 
        self.lbl_nbvalues['text']=text='Nb of points :\n'            \
                                 +str(self.ZeRankMax-self.ZeRankMin+1)\
                                 +'\n(max = '+str(keepRankMax+1)+')'
        self.lbl_XvalueRkMin['text']=str(Xtmptuple[4][self.ZeRankMin])
        self.lbl_XvalueRkMax['text']=str(Xtmptuple[4][self.ZeRankMax])
        self.lbl_YvalueRkMin['text']=str(Ytmptuple[4][self.ZeRankMin])
        self.lbl_YvalueRkMax['text']=str(Ytmptuple[4][self.ZeRankMax])
                
        # Axis name
        self.ZeXaxis=str(self.entr_Xaxname.get()) # Get the value of the field
        self.ZeYaxis=str(self.entr_Yaxname.get()) # Get the value of the field

        # Resolution (en px)
        Ppcm2dpi = 1.0/2.54
        if len(str(self.entr_ppp.get()))<1:
            self.ppp = 260
        else:
            self.ppp = int(self.entr_ppp.get())
        #EndIf
        
        if len(str(self.entr_Width.get()))<1:
            self.sizeX = 8 * Ppcm2dpi # * self.ppp
        else :
            self.sizeX = float(self.entr_Width.get()) * Ppcm2dpi # * self.ppp
        #EndIf

        if len(str(self.entr_Height.get()))<1:
            self.sizeY = 5 * Ppcm2dpi # * self.ppp
        else :
            self.sizeY = float(self.entr_Height.get()) * Ppcm2dpi # * self.ppp
        #EndIf

        # Update the window
        self.update()

        print("debuuuuuuuuuuuug ",self.sizeX,self.sizeY,self.ppp)
        
        # Set the tuple
        tupleT4F=[self.Zetitle,\
                  self.Zelegend,\
                  self.ZeXaxis,\
                  self.ZeYaxis,\
                  self.ZeRankMin,\
                  self.ZeRankMax,\
                  self.sizeX,\
                  self.sizeY,\
                  self.ppp\
        ]
    #EndDef


    def IHM_newCurve(self,event):
        global boolAddCurve
        boolAddCurve = True                  # We want to add another plot
        if self.izok:
            self.master.destroy()#quit the window
        else:
            self.button_apply['bg']='red'
        #EndIf
    #EndDef
    
    def IHM_displayCurve(self,event):        # We want NOT add another plot
        global boolAddCurve
        boolAddCurve = False
        if self.ZeRankMin>self.ZeRankMax:
            exit()
        #EndIf
        if self.izok:
            self.master.destroy()#quit the window
        else:
            self.button_apply['bg']='red'
        #EndIf
    #EndDef

######################################################################
######################################################################
class MyGUI(tk.Frame):
    # Cette classe sert aux etapes 1 et 2
    # * fichier
    # * colonne
    # * nom de l'axe

    # Variables of the object
    nbcol      = 0
    nblines    = 0
    Zefilename = ''
    Zecol      = -1
    Zesamefile = False
    inputfilename = ''
    def __init__(self, master,step,inputfn=''):
        self.inputfilename=inputfn
        if step==1:
            ZeAxis = 'x'
        else:
            ZeAxis = 'y'
        #EndIf
        
        # Windows Initialization
        tk.LabelFrame.__init__(self, master, height=400, width=450,\
                               text="Step : "                      \
                               +str(step)                          \
                               +"/"                                \
                               +str(nbstep)                        \
                               +' :    '                           \
                               + 'Choose the file for the '        \
                               +ZeAxis                             \
                               +' data',                           \
        )

        # # # # # # GUI # # # # # #
        #---Fields-----------------------------------------------------
      
        #---Buttons---

        #---Labels---
        # Total number of columns......................................
        self.lbl_nbColumns=tk.Label(self,                             \
                                    text="There is "                  \
                                        +str(self.nbcol)              \
                                        +" columns",                  \
                                    justify='center'                  \
        )
        self.lbl_nbColumns.grid(row=5, column=1,columnspan=4)

        # Help about how to use........................................
        self.lbl_infostep=tk.Label(self,                              \
                                   text='Be careful : clic on '       \
                                       +'\'Applicate\'\n'             \
                                       +'before going to the'         \
                                       + 'next step !',               \
                                   fg='red',bg='black',               \
                                   width=30                           \
        )
        self.lbl_infostep.grid(row=9,column=2,columnspan=3,rowspan=2)

        # Confirm the selected column..................................
        self.lbl_ZedisplayCol=tk.Label(self,                          \
                                       text="Column :  ",             \
                                       justify='center'               \
        )
        self.lbl_ZedisplayCol.grid(row=7, column=1,columnspan=4)

        # Confirm the display file.....................................
        self.lbl_Zedisplayfile=tk.Label(self,                         \
                                        text='',                      \
                                        justify='center'              \
        )
        self.lbl_Zedisplayfile.grid(row=4, column=1,columnspan=4)

                
       

        #---Buttons----------------------------------------------------
        # Choose the file..............................................
        self.button_chooseFile=tk.Button(self,                        \
                                         text="Choose a file",        \
                                         command=self.IHM_chooseFile  \
        )
        self.button_chooseFile.focus()
        self.button_chooseFile.grid(row=3, column=1)

        # Use the same file ?..........................................
        if len(self.inputfilename)>1:
            # If there is no file, we can not use again it
            self.tic_usesamefile = tk.Button(self,                     \
                                             text="Use the same file for "\
                                             +ZeAxis                  \
                                             +" than for x ?"         \
            )
            self.tic_usesamefile.bind("<Button-1>",self.IHM_samefile)
            self.tic_usesamefile.grid(row=3,column=2,columnspan=3)
        #EndIf
        
        #--- List------------------------------------------------------
        # List of columns..............................................
        self.list_chooseCol=tk.Listbox(self,width=10,height=5)
        for i in range(1,self.nbcol+1): # Construction of the list
            self.list_chooseCol.insert(i, 'column '+str(i))
        #EndFor
        self.list_chooseCol.bind('<Double-Button-1>',self.IHM_apply)
        self.list_chooseCol.grid(row=6, column=2)

        
        #---Buttons----------------------------------------------------
        # Save changements.............................................
        self.button_apply=tk.Button(self,                             \
                                    text="Applicate"                  \
        )
        self.button_apply.bind('<Button-1>',self.IHM_apply)
        self.button_apply.grid(row=8,column=2)
        
        # Next screen..................................................
        self.button_next=tk.Button(self,text="Next",                  \
                                   command=self.IHM_next              \
        )
        self.button_next.grid(row=8,column=4)

        # Previous..................THIS FUNCTION DO NOT WORK..........
        self.button_previous=tk.Button(self,                          \
                                       text="Previous",               \
                                       fg='grey'                      \
        )
        self.button_previous.bind("<Button-1>",self.IHM_previous)
        self.button_previous.grid(row=8,column=1)

        # Exit.........................................................
        self.button_exit=tk.Button(self,                              \
                                   text="Exit",                       \
                                   command=exit                   \
        ) #self.master.destroy        \
        self.button_exit.grid(row=10, column=1)

        #---Formating the window---------------------------------------
        self.disp_space=tk.Canvas(self,width=1,height=35)
        self.disp_space.grid(row=9,column=1,columnspan=4)
        self.disp_space.grid(row=2,column=1,columnspan=4)

        #---Update-----------------------------------------------------
        self.update()
    #EndDef

    def clear_text(self):
        #self.entry.delete(0, 'end')
        self.lbl_infostep['text']='Be careful : clic on \'Applicate\'\nbefore going to the next step !'
        self.lbl_infostep['fg']='red'
        self.lbl_infostep['bg']='black'

        
    ########################## Definitions IHM #############################


    def IHM_chooseFile(self):
        
        self.update()
        self.Zefilename = askopenfilename(filetypes=[\
                                                     ("Text files","*.txt"),\
                                                     ("Text files","*.text"),\
                                                     ("All files","*")\
        ])
        #("Data files","*.csv"),("Data files","*.dat"), # NE GERE PLUS LES CSV !!!
        self.lbl_Zedisplayfile['text'] = self.Zefilename
        self.IHM_loadcolumns()
        
    def IHM_loadcolumns(self):
        if len(self.Zefilename)>1 :
            L = []
            listtmp = []
            ttmp = []
            try:
                Zefile = open(self.Zefilename, "r")
                try:
                    readercsv = csv.reader(Zefile, delimiter=';')
                    listtmpcsv = list(readercsv) # here, we are
                                        
                    self.nblines = len(listtmpcsv)
                    self.nbcol = len(listtmpcsv[0][0].split())
                    
                    for i in range(0,self.nblines):
                        ttmp = []
                        for j in range(0,self.nbcol):
                            ttmp.append(float(listtmpcsv[i][0].split()[j]))
                        listtmp.append(ttmp)

                    self.lbl_ZedisplayCol['text'] = "Column : "
                    self.lbl_ZedisplayCol['bg'] = "silver"
                    self.update()
                except :
                    print('')#A nettoyer...
                finally:
                    print ('')#A nettoyer...
                #EndTry
            finally:
                Zefile.close()
            #EndTry
        #EndIf

        if self.nbcol<=0:
            self.lbl_ZedisplayCol['text'] = "Please choose another file "
            self.lbl_ZedisplayCol['bg'] = "red"
            self.update()
        #EndIf

        self.list_chooseCol.delete(0,self.nbcol)
        if(self.nbcol>0):
            self.lbl_nbColumns['text']="There is "+str(self.nbcol)+" columns"
            for i in range(1,self.nbcol+1):
                self.list_chooseCol.insert(i, 'column '+str(i))
            #EndFor
        else:
            self.lbl_ZedisplayCol['text'] = "Please choose another file "
            self.lbl_ZedisplayCol['bg'] = "red"
        #EndIf
        self.update()

    def IHM_apply(self,event):
        izok = True

        if(len(self.list_chooseCol.curselection())>0):
            self.Zecol = int(self.list_chooseCol.curselection()[0])+1
            self.lbl_ZedisplayCol['text'] = "Column : "+str(self.Zecol)
        else:
            self.Zecol = -1 # Error !
            self.lbl_ZedisplayCol['text'] = "Please choose a column"
            self.list_chooseCol['bg'] = "red"
            izok = False
        #EndIf
        self.update()

        if (len(self.Zefilename) == 1):
            self.lbl_Zedisplayfile['text'] = "Please choose a file"
            self.button_chooseFile['bg'] = "red"
            izok = False
        #EndIf
        if izok :
            self.lbl_infostep['text'] = "Ready for the next step !\nPlease select \'Next\'"
            self.lbl_infostep['fg'] = 'green'
        #EndIf
      
    
   
    
    def IHM_next(self):
        if(len(self.Zefilename)<=1):
            self.lbl_Zedisplayfile['text'] = "Please choose a file"
            self.button_chooseFile['bg'] = "red"
            self.tic_usesamefile['bg'] = "pink"
        elif(self.Zecol<=0):
            self.lbl_ZedisplayCol['text'] = "Please choose a column"
            self.list_chooseCol['bg'] = "red"
        else:
            global tupleGUI
            tupleGUI = [self.Zefilename, self.Zecol, self.nbcol, self.nblines]
            self.master.destroy()
        #EndIf
    


    def IHM_previous(self,event):
#        self.clear_text()
        print('prev')
        
    def IHM_samefile(self,event):
        if ( len(self.inputfilename) > 1 ):
            if( not(self.Zesamefile) ):
                self.Zesamefile = True #We choose the same file
                self.Zefilename=self.inputfilename
                self.lbl_Zedisplayfile['text'] = self.Zefilename
                self.button_chooseFile.grid_forget()
            else:
                self.Zesamefile = False #We coose another file
                self.button_chooseFile.grid(row=3, column=1)
                self.button_chooseFile.focus()
            #EndIf
        self.IHM_loadcolumns()
        self.update()
    #EndIf

########################################################################
def file2tuple(inputFile,inputcol,nbvalues):
    # IN  : inputFile (str)
    #       inputcol  (int)
    #       nbvalues  (int)
    # Out : Zecoor=[X0,X1,...,Xn]
    Zecoor = []
    inputcol = inputcol-1
    try:
        Zefile = open(inputFile, "r")
        try:
            readercsv = csv.reader(Zefile, delimiter=';')
            listtmpcsv = list(readercsv)
        
            firstline = 0 # compter le nombre de  lignes de commentaires
            #plot('inpu'+str(inputcol))
            for i in range(0,nbvalues):
                Zecoor.append(float(listtmpcsv[i][0].split()[inputcol]))
            #EndFor
        finally:
            Zefile.close()
    finally:
         return(Zecoor)
    exit(0)
    
####################################################

def inputData(): #Ok
    global Xtmptuple
    global Ytmptuple
    global tupleT4F
    global keepFileName
    
    Xtmptuple    = []
    Ytmptuple    = []
    tupleT4F     = []

    # ------------------ Step 1 ------------------------
    ZeWindowS1 = tk.Tk()
    # After the 1st while, the previous filename is suggested
    if len(keepFileName)<=1 :
        MyGUI(ZeWindowS1,1).pack(expand=True, fill='both')
    else:
        MyGUI(ZeWindowS1,1,keepFileName).pack(expand=True, fill='both')
    #EndIF
    ZeWindowS1.mainloop() #Boucler sur l IHM : le laisser a la fin !!!
    Xtmptuple=tupleGUI
    # ------------------ End Step 1 --------------------
    keepFileName = Xtmptuple[0] # The suggested filename is updated

    # ------------------ Step 2 ------------------------
    ZeWindowS2 = tk.Tk()
    MyGUI(ZeWindowS2,2,keepFileName).pack(expand=True, fill='both')
    ZeWindowS2.mainloop()
    Ytmptuple=tupleGUI
    # ------------------ End Step 2 --------------------
    
    # We concatenate all results in one tuple for the export
    Xtmptuple.append(file2tuple(Xtmptuple[0],Xtmptuple[1],Xtmptuple[3]))
    Ytmptuple.append(file2tuple(Ytmptuple[0],Ytmptuple[1],Ytmptuple[3]))

    # ------------------ Step 3 ------------------------
    ZeWindowS3 = tk.Tk()
    Text4Figure(ZeWindowS3,             \
                Xtmptuple,              \
                Ytmptuple               \
    ).pack(expand=True, fill='both')
    ZeWindowS3.mainloop()
    
    for i in range(0,9):
        if (i!=3)and(i!=7):
            Xtmptuple.append(tupleT4F[i])
            print("ddddebbbbbbug : ",tupleT4F[i])
        if (i!=2)and(i!=6):
            Ytmptuple.append(tupleT4F[i])
    #EndFor
    # ------------------ End Step 3 --------------------
#EndDef

def main():
    #Patern
    # The tuple form :
    # Xtuple = [ (idem Ytuple)
    #     ['filename', zecol, nbcol, nblines,[values],
    #      'title' , 'legend', 'Xaxisname', rkMIN, rkMAX, sizeX(in),ppp]
    #       ...]
    
    # Define global variables
    global Xtuple        # tuple containing all datas for plot
    global Ytuple
    global boolAddCurve  # boolean for looping on inputData() to add curves
    global nbCurve       # nb of curves
    global keepFileName
    global nbstep
    
    Xtuple       = []
    Ytuple       = []

    Xaxis        = ''
    Yaxis        = ''
    
    boolAddCurve = True
    nbCurve      = 1
    keepFileName = '' #This is for inputData()

    nbstep= 3
    
    ListOfColors=['blue','red', 'green', 'purple', 'orange', 'navy'] #6
    nbcolors = 6

    thereizalegend = False
    cpt = 1
    while boolAddCurve:
        inputData()
        Xtuple.append(Xtmptuple)
        Ytuple.append(Ytmptuple)

        print('This is the '+str(cpt)+' curve')
        cpt = cpt+1
    #EndWhile

    if nbcolors<len(Xtuple):
        print('Only, the first '+str(nbcolors)+' will be print in color !')

    # PLOT :


    title    = Xtuple[0][5]
    sizeX    = Xtuple[0][10]
    sizeY    = Ytuple[0][10]
    Resolppp = Xtuple[0][11]      
    Zecolor = ''


    fig=plt.figure(figsize=(sizeX, sizeY), dpi=Resolppp)
    plt.rc('text', usetex=BOOL_LATEX)    # Use TeX formating for text
    #    plt.rc('font', family='serif')

    for i in range(0,min(nbcolors,len(Xtuple))):
        if i<len(Xtuple):
            Zecolor = ListOfColors[i]
        else :
            Zecolor = 'black'
        #EndIf
        
        Xvalues  = Xtuple[i][4]
        Yvalues  = Ytuple[i][4]

        Xaxis    = Xaxis+''+Xtuple[i][7]
        Yaxis    = Yaxis+''+Ytuple[i][7]
        rankMin = Xtuple[i][8]
        rankMax = Xtuple[i][9]

        legend   = Xtuple[i][6]
        thereizalegend = (thereizalegend)or(len(legend)>1)
        plt.plot(Xvalues[int(rankMin):int(rankMax)],\
                 Yvalues[int(rankMin):int(rankMax)],\
                 'bo-', linewidth=1,markersize=1.3, \
                 color=Zecolor,\
                 label=legend\
        )

    #EndFor

    if thereizalegend:
            plt.legend(bbox_to_anchor=(1.04,1),\
                       loc="upper left",\
                       mode="expand",\
                       borderaxespad=0\
            )
    #EndIf
    plt.title(title)
    plt.xlabel(Xaxis)
    plt.ylabel(Yaxis,fontsize=16)

    
    #fig.set_size_inches(width,height)
    # Taille du graphic dans la fenetre
    botfig = 0
    leffig = 0
    topfig = 1
    rigfig = 1
    if len(title)>1:
        topfig = 0.90
    if thereizalegend:
        rigfig = 0.83
    plt.tight_layout(rect=[leffig,botfig,rigfig,topfig])

        
    plt.show()
    plt.close('all')
######################################################################

#Welcome message

# Les 2 premieres ligne seront toujours une description, commencant par #
# Ordre : Nombre de lignes, Nombre de colones


print("Welcome in my program !")
#samefile=0

main()

exit()


#End "From here all work"
